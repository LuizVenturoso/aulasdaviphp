<?php

class Colaborador {

    private $cpf;
    private $rg;
    private $nome;
    private $cargo;
    private $salarioBruto;
    protected $desconto;
    private $salarioLiquido;
    private $vrlDesconto;

    public function __construct() {
        $this->setCargo("Não Definido");
        $this->setCpf("000.000.000-00");
        $this->setNome("Não Informado");
        $this->setRg("000000");
        $this->setDesconto(0);
        $this->setSalarioBruto("Sem Salario");
    }

    function getCpf() {
        return $this->cpf;
    }

    function getSalarioBruto() {
        return $this->salarioBruto;
    }

    function getDesconto() {
        return $this->desconto;
    }

    function setSalarioBruto($salarioBruto) {
        $this->salarioBruto = $salarioBruto;
    }

    function setDesconto($desconto) {
        $this->vrlDesconto = "Sem desconto";
        $this->salarioLiquido = "Sem Salario";
        $this->desconto = "Sem desconto";
    }

    function getRg() {
        return $this->rg;
    }

    function getNome() {
        return $this->nome;
    }

    function getCargo() {
        return $this->cargo;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setRg($rg) {
        $this->rg = $rg;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    function Trabalhando() {
        return "Não está Trabalhando!";
    }

    function getSalarioLiquido() {
        return $this->salarioLiquido;
    }

    function getVlrDesonto() {
        return $this->vrlDesconto;
    }

    function setVrlDesconto($vrlDesconto) {
        $this->vrlDesconto = $vrlDesconto;
    }

    function setSalarioLiquido($salarioLiquido) {
        $this->salarioLiquido = $salarioLiquido;
    }

    function getDadosCompleto() {
        echo "Nome: " . $this->getNome() . "<br>";
        echo "CPF: " . $this->getCpf() . "<br>";
        echo "RG: " . $this->getRg() . "<br>";
        echo "Salario Bruto: " . $this->getSalarioBruto() . "<br>";
        echo "Percentual de desconto: " . $this->getDesconto() . "<br>";
        echo "Salario Liquido: " . $this->getSalarioLiquido() . "<br>";
        echo "O que está fazendo? " . $this->Trabalhando() . "<br>";
    }

}
