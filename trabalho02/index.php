<?php
require_once './autoLoad.php';

echo "------------- Classe Pai Colaborador ------------- <br>";
$colaborador = new Colaborador();
$colaborador->getDadosCompleto();

echo "<br><br> -------------  Classe Gerente ------------- <br>";
$gerente = new Gerente("000.000.000-00", "Luiz", "0000000");
$gerente->getDadosCompleto();

echo "<br><br> -------------  Classe Suporte ------------- <br>";
$Suporte = new Suporte("111.111.111.11", "Matheus", "111111");
$Suporte->getDadosCompleto();

echo "<br><br> -------------  Classe Badeco ------------- <br>";
$Badeco = new Badeco("222.222.222.22", "Felipe", "222222");
$Badeco->getDadosCompleto();

echo "<br><br> -------------  Classe Financeiro ------------- <br>";
$Financeiro = new Financeiro("444.444.444.44", "Joice", "44444");
$Financeiro->getDadosCompleto();


?>
