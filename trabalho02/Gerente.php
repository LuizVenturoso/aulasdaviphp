<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gerente
 *
 * @author NoteLuiz
 */
class Gerente extends Colaborador {

    public function __construct($cpf, $nome, $rg) {
        $this->setCargo("Gerente");
        $this->setCpf($cpf);
        $this->setNome($nome);
        $this->setRg($rg);
        $this->setSalarioBruto(3500.00);
        $this->setDesconto(20);
    }

    public function Trabalhando() {
        Return "Está gerenciando";
    }

    function setDesconto($desconto) {
        $this->setVrlDesconto(($this->getSalarioBruto() / 100) * $desconto);
        $this->setSalarioLiquido($this->getSalarioBruto() - $this->getVlrDesonto());
        $this->desconto = $desconto;
    }
    function getDadosCompleto() {
        echo "Nome: " . $this->getNome() . "<br>";
        echo "CPF: " . $this->getCpf() . "<br>";
        echo "RG: " . $this->getRg() . "<br>";
        echo "Cargo: " . $this->getCargo() . "<br>";
        echo "Salario Bruto: " . $this->getSalarioBruto() . "<br>";
        echo "Percentual de desconto: " . $this->getDesconto() . "%<br>";
        echo "Valor descontado: " . $this->getVlrDesonto() . "<br>";
        echo "Salario Liquido: " . $this->getSalarioLiquido() . "<br>";
        echo "O que está fazendo? " . $this->Trabalhando() . "<br>";
    }

}
