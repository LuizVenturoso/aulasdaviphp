<?php

class Carro {

    private $cor;
    private $anoFab;
    private $anoModelo;
    private $modelo;
    private $motor;

    public function __construct($cor, $anoFab, $anoModelo, $modelo, $motor) {
        $this->setAnoFab($anoFab);
        $this->setAnoModelo($anoModelo);
        $this->setCor($cor);
        $this->setModelo($modelo);
        $this->setMotor($motor);
    }

    public function getCarroCompleto() {
        echo "Modelo: {$this->getModelo() } <br>";
        echo "Ano Modelo: {$this->getAnoModelo() } <br>";
        echo "Ano Fabricação: {$this->getAnoFab() } <br>";
        echo "Cor: {$this->getCor() } <br>";
        echo "Motor: {$this->getMotor() } <br>";
    }

    function getCor() {
        return $this->cor;
    }

    function getAnoFab() {
        return $this->anoFab;
    }

    function getAnoModelo() {
        return $this->anoModelo;
    }

    function getModelo() {
        return $this->modelo;
    }

    function getMotor() {
        return $this->motor;
    }

    function setCor($cor) {
        $this->cor = $cor;
    }

    function setAnoFab($anoFab) {
        $this->anoFab = $anoFab;
    }

    function setAnoModelo($anoModelo) {
        $this->anoModelo = $anoModelo;
    }

    function setModelo($modelo) {
        $this->modelo = $modelo;
    }

    function setMotor($motor) {
        $this->motor = $motor;
    }

}

?>
